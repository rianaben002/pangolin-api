const bcrypt = require('bcrypt');
const Pangolin = require('../models/pangolin.model');
const jwt = require('jsonwebtoken');
const _ = require("lodash");
const mongoose = require('mongoose')

exports.register = (req, res) => {
    bcrypt.hash(req.body.password, 10)
        .then((pswHash) => {
            const user = new Pangolin({
                name: req.body.name,
                username: req.body.username,
                password: pswHash,
                role: req.body.role === '' ? 'Guérrier' : req.body.role,
                friends: []
            })
            user.save()
                .then(() => {
                    res.status(201).json({
                        status: true,
                        message: 'Pangolin registered successfully'
                    })
                })
                .catch(err => {
                    res.status(500).json({
                        status: false,
                        message: err
                    })
                })
        }).catch((err) => {
            res.status(500).json({
                status: false,
                message: err
            })
        });
};

exports.login = (req, res) => {
    Pangolin.findOne({ username: req.body.username })
        .populate({
            path: 'friends'
        })
        .then(pangolin => {
            if (!pangolin) {
                return res.status(404).json({
                    status: false,
                    message: 'Pangolin not found'
                })
            }
            // pangolin username exist
            bcrypt.compare(req.body.password, pangolin.password)
                .then(valid => {
                    // console.log('Valid : ', valid);
                    if (!valid) {
                        return res.status(401).json({
                            status: false,
                            message: 'Incorrect password'
                        })
                    }
                    // credential valid
                    res.status(200).json({
                        status: true,
                        data: pangolin,
                        token: jwt.sign({ pangolinId: pangolin._id },
                            'PANGOLIN_TOKEN_SECRET', { expiresIn: '24h' })
                    })
                })
                .catch(err => {
                    console.log('Err : ', err);
                    res.status(401).json({
                        status: false,
                        message: err
                    })
                })
        })
        .catch(err => res.status(401).json({
            status: false,
            message: err
        }))
};

exports.updatePangolin = (req, res) => {
    const token = req.header('Authorization').split(' ')[1];
    console.log(req.header('Authorization'));
    const data = jwt.verify(token, 'PANGOLIN_TOKEN_SECRET');
    Pangolin.findOne({ _id: data.pangolinId })
        .populate({
            path: 'friends'
        })
        .then(pangolin => {
            pangolin.role = req.body.role;
            pangolin.name = req.body.name;
            pangolin.username = req.body.username;
            pangolin.save()
                .then(() => res.status(200).json({
                    status: true,
                    message: 'Role pangolin update successfully',
                    data: pangolin
                }))
                .catch(err => {
                    res.status(400).json({
                        status: false,
                        message: err
                    })
                })
        })
        .catch(err => {
            res.status(404).json({
                status: false,
                message: err
            })
        })
};

exports.addFriend = (req, res) => {
    const token = req.header('Authorization').split(' ')[1];
    const data = jwt.verify(token, 'PANGOLIN_TOKEN_SECRET');
    Pangolin.findOne({ _id: data.pangolinId })
        .populate({
            path: 'friends'
        })
        .then(pangolin => {
            Pangolin.findOne({ _id: req.body.pangolinId })
                .then(p => {
                    pangolin.friends.push(p);
                    pangolin.save()
                        .then(() => {
                            res.status(200).json({
                                status: true,
                                message: 'Pangolin friend added successfully',
                                data: pangolin
                            })
                        })
                        .catch(err => {
                            res.status(400).json({
                                status: false,
                                message: err
                            })
                        })
                })
                .catch(err => {
                    res.status(404).json({
                        status: false,
                        message: 'ERROR_' + err
                    })
                })


        })
        .catch(err => {
            res.status(404).json({
                status: false,
                message: 'ERROR__' + err
            })
        })
};

exports.deleteFriend = (req, res) => {
    console.log('deleting...');
    const token = req.header('Authorization').split(' ')[1];
    const data = jwt.verify(token, 'PANGOLIN_TOKEN_SECRET');
    Pangolin.findOne({ _id: data.pangolinId })
        .populate({
            path: 'friends'
        })
        .then(pangolin => {
            pangolin.friends = pangolin.friends.filter(p => {
                if (String(req.body.pangolinId) != p._id) {
                    return p;
                }
            });

            pangolin.save()
                .then(() => res.status(200).json({
                    status: true,
                    message: 'Pangolin friend removed successfully',
                    data: pangolin
                }))
                .catch(err => {
                    res.status(400).json({
                        status: false,
                        message: err
                    })
                })
        })
        .catch(err => {
            res.status(404).json({
                status: false,
                message: 'Pangolin not found ' + err
            })
        })
};

exports.getPangolinUnkown = (req, res) => {
    const token = req.header('Authorization').split(' ')[1];
    const data = jwt.verify(token, 'PANGOLIN_TOKEN_SECRET');
    Pangolin.findOne({ _id: data.pangolinId })
        .populate({
            path: 'friends'
        })
        .then((pangolin) => {
            let already_friends = pangolin.friends
                // push the current pangolin in the filtre
            already_friends.push(pangolin)

            // get all pangolin without in array filtre
            Pangolin.find({ _id: { $nin: already_friends } })
                .then(result => {
                    res.status(200).json({
                        status: true,
                        message: 'Get not friends successfully',
                        data: result
                    })
                })
                .catch(err => res.status(404).json({
                    status: false,
                    message: err
                }))
        })
        .catch(err => res.status(404).json({
            status: false,
            message: err
        }))
};

exports.getUserWithToken = (req, res) => {
    console.log('Ohéé');
    const token = req.header('Authorization').split(' ')[1];
    const data = jwt.verify(token, 'PANGOLIN_TOKEN_SECRET');
    console.log('Hello', data);
    Pangolin.findOne({ _id: req.body.pangolinId })
        .populate({
            path: 'friends'
        })
        .then(result => {
            res.status(200).json({
                status: true,
                message: "Refresh current pangolin",
                data: result
            })
        })
        .catch(err => res.status(404).json({
            status: false,
            message: err
        }))
};