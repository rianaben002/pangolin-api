const express = require('express');
const router = express.Router();

const pangolinController = require('../controllers/pangolin.controller');

router.post('/login', pangolinController.login);
router.post('/register', pangolinController.register);
router.post('/updatePangolin', pangolinController.updatePangolin);
router.post('/addFriend', pangolinController.addFriend);
router.post('/deleteFriend', pangolinController.deleteFriend);
router.get('/list', pangolinController.getPangolinUnkown);
router.post('/refreshCurrentPangolin', pangolinController.getUserWithToken);

module.exports = router;