const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const app = express();
const pangolinRoutes = require('./routes/pangolin.route');

mongoose.connect('mongodb://ben:123456789@localhost:27017/?authMechanism=DEFAULT', {
        // useNewUrlParser: true,
        // useUnifiedTopology: true
    })
    .then(() => console.log('Connexion à mongodb réussie !'))
    .catch(() => console.log('Connexion à mongodb échouée !'))

app.use(bodyParser.json());


app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use('/api/pangolin', pangolinRoutes);

module.exports = app;