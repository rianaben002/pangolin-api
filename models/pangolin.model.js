const mongoose = require('mongoose');

const Pangolin = mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    username: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    role: {
        type: String,
        require: false,
        default: 'Guerrier'
    },
    friends: [{
        type: mongoose.Types.ObjectId,
        ref: 'Pangolin'
    }]
});

module.exports = mongoose.model('Pangolin', Pangolin);